<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ganadores".
 *
 * @property int $id
 * @property int|null $id_competiciones
 * @property int|null $num_ganadores
 *
 * @property Competiciones $competiciones
 */
class Ganadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ganadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_competiciones', 'num_ganadores'], 'integer'],
            [['id_competiciones'], 'exist', 'skipOnError' => true, 'targetClass' => Competiciones::className(), 'targetAttribute' => ['id_competiciones' => 'id_competiciones']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_competiciones' => 'Id Competiciones',
            'num_ganadores' => 'Num Ganadores',
        ];
    }

    /**
     * Gets query for [[Competiciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompeticiones()
    {
        return $this->hasOne(Competiciones::className(), ['id_competiciones' => 'id_competiciones']);
    }
}
