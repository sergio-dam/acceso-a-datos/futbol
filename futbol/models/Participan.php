<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participan".
 *
 * @property int $id_participan
 * @property int|null $id_equipos
 * @property int|null $id_competiciones
 *
 * @property Competiciones $competiciones
 * @property Equipos $equipos
 */
class Participan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_equipos', 'id_competiciones'], 'integer'],
            [['id_competiciones'], 'exist', 'skipOnError' => true, 'targetClass' => Competiciones::className(), 'targetAttribute' => ['id_competiciones' => 'id_competiciones']],
            [['id_equipos'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['id_equipos' => 'id_equipos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_participan' => 'Id Participan',
            'id_equipos' => 'Id Equipos',
            'id_competiciones' => 'Id Competiciones',
        ];
    }

    /**
     * Gets query for [[Competiciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompeticiones()
    {
        return $this->hasOne(Competiciones::className(), ['id_competiciones' => 'id_competiciones']);
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasOne(Equipos::className(), ['id_equipos' => 'id_equipos']);
    }
}
