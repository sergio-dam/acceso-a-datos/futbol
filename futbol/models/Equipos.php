<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $id_equipos
 * @property string|null $nombre
 * @property int|null $titulos
 * @property string|null $nom_presidente
 * @property string|null $nom_entrenador
 *
 * @property Estadios[] $estadios
 * @property Jugadores[] $jugadores
 * @property Participan[] $participans
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulos'], 'integer'],
            [['nombre', 'nom_presidente', 'nom_entrenador'], 'string', 'max' => 20],
            [['nombre', 'titulos'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_equipos' => 'Id Equipos',
            'nombre' => 'Nombre',
            'titulos' => 'Títulos',
            'nom_presidente' => 'Nombre del Presidente',
            'nom_entrenador' => 'Nombre del Entrenador',
        ];
    }

    /**
     * Gets query for [[Estadios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadios()
    {
        return $this->hasMany(Estadios::className(), ['id_equipos' => 'id_equipos']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::className(), ['id_equipos' => 'id_equipos']);
    }

    /**
     * Gets query for [[Participans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipans()
    {
        return $this->hasMany(Participan::className(), ['id_equipos' => 'id_equipos']);
    }
}
