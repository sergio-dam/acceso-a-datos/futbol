<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competiciones".
 *
 * @property int $id_competiciones
 * @property string|null $nombre
 * @property string|null $participantes
 * @property string|null $nom_ganador
 * @property int|null $numero_veces
 *
 * @property Ganadores[] $ganadores
 * @property Participan[] $participans
 */
class Competiciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competiciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_veces'], 'integer'],
            [['nombre', 'participantes'], 'string', 'max' => 20,],
            [['nom_ganador'], 'string', 'max' => 30],
            [['nombre', 'participantes'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_competiciones' => 'Id Competiciones',
            'nombre' => 'Nombre',
            'participantes' => 'Participantes',
            'nom_ganador' => 'Nombre del Ganador',
            'numero_veces' => 'Numero de Veces',
        ];
    }

    /**
     * Gets query for [[Ganadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGanadores()
    {
        return $this->hasMany(Ganadores::className(), ['id_competiciones' => 'id_competiciones']);
    }

    /**
     * Gets query for [[Participans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipans()
    {
        return $this->hasMany(Participan::className(), ['id_competiciones' => 'id_competiciones']);
    }
}
