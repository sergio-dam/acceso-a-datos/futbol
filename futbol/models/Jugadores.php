<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $id_jugadores
 * @property string|null $nombre
 * @property int|null $edad
 * @property string|null $posicion
 * @property int|null $pjugados
 * @property int|null $num_goles
 * @property int|null $num_asistencias
 * @property int|null $num_goles_encajados
 * @property int|null $id_equipos
 *
 * @property Equipos $equipos
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['edad', 'pjugados', 'num_goles', 'num_asistencias', 'num_goles_encajados', 'id_equipos'], 'integer'],
            [['nombre', 'posicion'], 'string', 'max' => 20],
            [['nombre', 'posicion'], 'required'],
            //[['id_equipos'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['id_equipos' => 'id_equipos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jugadores' => 'Id Jugadores',
            'nombre' => 'Nombre',
            'edad' => 'Edad',
            'posicion' => 'Posición',
            'pjugados' => 'Partidos Jugados',
            'num_goles' => 'Número de Goles',
            'num_asistencias' => 'Número de Asistencias',
            'num_goles_encajados' => 'Goles Encajados',
            'id_equipos' => 'Id Equipos',
        ];
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasOne(Equipos::className(), ['id_equipos' => 'id_equipos']);
    }
}
