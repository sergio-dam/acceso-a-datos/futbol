<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estadios".
 *
 * @property int $id_estadios
 * @property string|null $nombre
 * @property string|null $ciudad
 * @property int|null $capacidad
 * @property int|null $fecha_construccion
 * @property int|null $id_equipos
 *
 * @property Equipos $equipos
 */
class Estadios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estadios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['capacidad', 'fecha_construccion', 'id_equipos'], 'integer'],
            [['nombre', 'ciudad'], 'string', 'max' => 20],
            [['id_equipos'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['id_equipos' => 'id_equipos']],
            [['nombre', 'ciudad'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_estadios' => 'Id Estadios',
            'nombre' => 'Nombre',
            'ciudad' => 'Ciudad',
            'capacidad' => 'Capacidad',
            'fecha_construccion' => 'Fecha de Construcción',
            'id_equipos' => 'Id Equipos',
        ];
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasOne(Equipos::className(), ['id_equipos' => 'id_equipos']);
    }
}
