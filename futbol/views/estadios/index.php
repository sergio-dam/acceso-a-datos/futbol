<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estadios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estadios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Estadios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
       

        
            'nombre',
            'ciudad',
            'capacidad',
            'fecha_construccion',
            //'id_equipos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
