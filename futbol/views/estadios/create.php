<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estadios */

$this->title = 'Nuevos Estadios';
$this->params['breadcrumbs'][] = ['label' => 'Estadios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estadios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
