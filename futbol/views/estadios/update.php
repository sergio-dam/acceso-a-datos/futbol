<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estadios */

$this->title = 'Actualizar Estadios: ' . $model->id_estadios;
$this->params['breadcrumbs'][] = ['label' => 'Estadios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_estadios, 'url' => ['view', 'id' => $model->id_estadios]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estadios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
