<?php
use yii\grid\GridView;

        
?>
<style type="text/css">
    h2{
        font-weight:bold;
    }
    body{
      
    }
   img{width:300px;
   height:250px;
   border:2px solid black;
   }
   table{
        border: black 2px solid;
   }
        </style>
 
<div class="jumbotron">  
    <img src="https://i.pinimg.com/originals/fa/ff/3a/faff3a3bdb230a1f35eb0b8f9c045bfc.jpg" alt="courtois" align="right">
  
    </br></br></br></br><h2>
        <?=$titulo?>
    </h2>
    
    <p class="lead">
        <?=$enunciado?>
    </p>
    
    

</div>

<?=GridView::widget([
    'dataProvider'=> $resultados,
    'columns'=>$campos,
]);?>

        <p>Thibaut Nicolas Marc Courtois (Bree, Limburgo; 11 de mayo de 1992) es un futbolista belga que juega como portero en el Real Madrid Club de Fútbol de la Primera División de España.​ Es internacional con la selección de fútbol de Bélgica. Debutó con 16 años con el Koninklijke Racing Club Genk en la Primera División de Bélgica con el que se proclamó campeón de Liga en 2011. Ese mismo año fichó por el Chelsea Football Club pero fue cedido al Atlético de Madrid durante tres temporadas. Con el equipo español conquistó una Europa League y una Supercopa de Europa en 2012, una Copa del Rey en 2013 y una Liga en 2014. Una vez de vuelta al Chelsea ganó la Copa de la Liga en 2015 y la Premier League en 2015 y 2017, además de una FA Cup en 2018. Ya en el Real Madrid logró un Mundial de Clubes en 2018 y una Supercopa de España en 2020.</br></br></p>
        
        <table>

  <tr>

      <th>K.R.C Genk</th>
    <td>2008-2011</td>
   
  </tr>

  <tr>
      <th>Atletico de Madrid</th>    

      <td>2011-2014</td>
 </tr>
 
  <tr>
      <th>Chelsea F.C.</th>
    <td>2014-2018</td> 
   </tr>
  
  <tr>
      <th>Real Madrid C.F.</th> 
       <td>2018-</td>
  </tr>
  
  

</table>