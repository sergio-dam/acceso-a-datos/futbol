<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */

$this->title = 'Crear Competiciones';
$this->params['breadcrumbs'][] = ['label' => 'Competiciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competiciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
