<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */

$this->title = 'Actualizar las Competiciones: ' . $model->id_competiciones;
$this->params['breadcrumbs'][] = ['label' => 'Competiciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_competiciones, 'url' => ['view', 'id' => $model->id_competiciones]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="competiciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
