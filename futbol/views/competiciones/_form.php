<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Competiciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="competiciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'participantes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nom_ganador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_veces')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
