<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Competiciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competiciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Competiciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            'nombre',
            'participantes',
            'nom_ganador',
            'numero_veces',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
