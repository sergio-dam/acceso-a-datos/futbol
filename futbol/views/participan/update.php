<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Participan */

$this->title = 'Update Participan: ' . $model->id_participan;
$this->params['breadcrumbs'][] = ['label' => 'Participans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_participan, 'url' => ['view', 'id' => $model->id_participan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="participan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
