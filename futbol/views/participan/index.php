<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Participans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Participan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_participan',
            'id_equipos',
            'id_competiciones',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
