<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Participan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_equipos')->textInput() ?>

    <?= $form->field($model, 'id_competiciones')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
