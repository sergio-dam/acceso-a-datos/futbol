<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jugadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Jugadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
          
            'nombre',
            'edad',
            'posicion',
            'pjugados',
            'num_goles',
            'num_asistencias',
            'num_goles_encajados',
         
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
