<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jugadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jugadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'posicion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pjugados')->textInput() ?>

    <?= $form->field($model, 'num_goles')->textInput() ?>

    <?= $form->field($model, 'num_asistencias')->textInput() ?>

    <?= $form->field($model, 'num_goles_encajados')->textInput() ?>

    <?= $form->field($model, 'id_equipos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
