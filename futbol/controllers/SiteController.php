<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Competiciones;
use app\models\Equipos;
use app\models\Estadios;
use app\models\Ganadores;
use app\models\Jugadores;
use app\models\Participan;
use app\models\Noticias;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
   
 public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct jugadores.nombre)jugadores,count(distinct edad),count(distinct posicion)posición,count(distinct pjugados)partidos_jugados,count(distinct num_goles)número_goles,count(distinct num_asistencias)número_asistencias,count(distinct num_goles_encajados)goles_encajados FROM jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos WHERE equipos.nombre='Real Madrid';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select jugadores.nombre AS jugadores,edad,posicion as posición,pjugados AS partidos_jugados,num_goles AS número_goles,num_asistencias AS número_asistencias,num_goles_encajados AS goles_encajados from jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos 
WHERE equipos.nombre='Real Madrid'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 30,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['jugadores','edad','posición','partidos_jugados','número_goles','número_asistencias','goles_encajados'],
            "titulo"=>"Real Madrid C.F.",
            "enunciado"=>"Plantilla 2019-2020",
           
        ]);
    }
    
  
    public function actionConsulta2 (){
        $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct jugadores.nombre)jugadores,count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias),count(distinct num_goles_encajados) FROM jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos WHERE equipos.nombre='FC Barcelona';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select jugadores.nombre AS jugadores,edad,posicion,pjugados,num_goles,num_asistencias,num_goles_encajados from jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos 
WHERE equipos.nombre='FC Barcelona'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 30,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['jugadores','edad','posicion','pjugados','num_goles','num_asistencias','num_goles_encajados'],
            "titulo"=>"FC Barcelona",
            "enunciado"=>"Plantilla 2019-2020",
           
        ]);
    }
    
    


     
    public function actionConsulta3(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct jugadores.nombre)jugadores,count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias),count(distinct num_goles_encajados) FROM jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos WHERE equipos.nombre='Atletico de Madrid';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select jugadores.nombre AS jugadores,edad,posicion,pjugados,num_goles,num_asistencias,num_goles_encajados from jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos 
WHERE equipos.nombre='Atletico de Madrid'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 30,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['jugadores','edad','posicion','pjugados','num_goles','num_asistencias','num_goles_encajados'],
            "titulo"=>"Atletico de Madrid",
            "enunciado"=>"Plantilla 2019-2020",
           
        ]);
    }  
    
      public function actionConsulta4(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct jugadores.nombre)jugadores,count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias),count(distinct num_goles_encajados) FROM jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos WHERE equipos.nombre='Valencia FC';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select jugadores.nombre AS jugadores,edad,posicion,pjugados,num_goles,num_asistencias,num_goles_encajados from jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos 
WHERE equipos.nombre='Valencia FC'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 30,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['jugadores','edad','posicion','pjugados','num_goles','num_asistencias','num_goles_encajados'],
            "titulo"=>"Valencia F.C",
            "enunciado"=>"Plantilla 2019-2020",
           
        ]);
    }  
    
      public function actionConsulta5(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct jugadores.nombre)jugadores,count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias),count(distinct num_goles_encajados) FROM jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos WHERE equipos.nombre='Sevilla FC';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select jugadores.nombre AS jugadores,edad,posicion,pjugados,num_goles,num_asistencias,num_goles_encajados from jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos 
WHERE equipos.nombre='Sevilla FC'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 30,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['jugadores','edad','posicion','pjugados','num_goles','num_asistencias','num_goles_encajados'],
            "titulo"=>"Sevilla F.C",
            "enunciado"=>"Plantilla 2019-2020",
           
        ]);
    }  
      public function actionConsulta6(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct jugadores.nombre)jugadores,count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias),count(distinct num_goles_encajados) FROM jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos WHERE equipos.nombre='Real Sociedad';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select jugadores.nombre AS jugadores,edad,posicion,pjugados,num_goles,num_asistencias,num_goles_encajados from jugadores LEFT JOIN equipos ON jugadores.id_equipos = equipos.id_equipos 
WHERE equipos.nombre='Real Sociedad'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 30,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['jugadores','edad','posicion','pjugados','num_goles','num_asistencias','num_goles_encajados'],
            "titulo"=>"Real Sociedad",
            "enunciado"=>"Plantilla 2019-2020",
           
        ]);
    }  
    
    public function actionConsulta7(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct enunciado) noticia FROM noticias where id='1';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select enunciado as noticia from noticias where id='1';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado1",[
            "resultados"=>$dataProvider,
            "titulo"=>"Benzema sigue solo",
            "enunciado"=>"Real Madrid: Único '9' apto para Zidane",
           
        ]);
    }  
    public function actionConsulta8(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct enunciado) noticia FROM noticias where id='2';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select enunciado as noticia from noticias where id='2';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado2",[
            "resultados"=>$dataProvider,
            "titulo"=>"La encrucijada de Ansu Fati",
            "enunciado"=>"FC Barcelona: Seguira en el primer equipo",
           
        ]);
    }  
    public function actionConsulta9(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct enunciado) noticia from noticias where id='3';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select enunciado as noticia from noticias where id='3';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado3",[
            "resultados"=>$dataProvider,
            "titulo"=>"La gran obra del Cholismo",
            "enunciado"=>"Se cumplen 6 años de la liga conquistada",
           
        ]);
    }  
    
     public function actionConsulta10(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre),count(distinct titulos),count(distinct nom_presidente),count(distinct nom_entrenador) FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=1;")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,titulos,nom_presidente,nom_entrenador FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=1;",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','titulos','nom_presidente','nom_entrenador'],
            "titulo"=>"Bundesliga",
            "enunciado"=>"Equipos 2019-2020",
           
        ]);
    }  
 public function actionConsulta11(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre),count(distinct titulos)títulos,count(distinct nom_presidente)nombre_del_presidente,count(distinct nom_entrenador)nombre_del_entrenador FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=7;")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,titulos as títulos,nom_presidente as nombre_del_presidente,nom_entrenador as nombre_del_entrenador FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=7;",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','títulos','nombre_del_presidente','nombre_del_entrenador'],
            "titulo"=>"LaLiga Santander",
            "enunciado"=>"Equipos 2019-2020",
           
        ]);
    }  
 public function actionConsulta12(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre),count(distinct titulos),count(distinct nom_presidente),count(distinct nom_entrenador) FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=2;")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,titulos,nom_presidente,nom_entrenador FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=2;",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','titulos','nom_presidente','nom_entrenador'],
            "titulo"=>"Serie A",
            "enunciado"=>"Equipos 2019-2020",
           
        ]);
    }  
public function actionConsulta13(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=5;")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=5;",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Copa del rey",
            "enunciado"=>"Equipos 2019-2020",
           
        ]);
    }  
    public function actionConsulta14(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=8;")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=8;",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Champions League",
            "enunciado"=>"Equipos 2019-2020",
           
        ]);
    }  
 public function actionConsulta15(){
        
            $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=10;")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre FROM equipos INNER JOIN participan ON equipos.id_equipos = participan.id_equipos
  WHERE id_competiciones=10;",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Premier League",
            "enunciado"=>"Equipos 2019-2020",
           
        ]);
    }  
    public function actionConsulta16(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion)posición,count(distinct pjugados)partidos_jugados,count(distinct num_goles_encajados)goles_encajados from jugadores where nombre='Thibaut Courtois';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion as posición,pjugados as partidos_jugados,num_goles_encajados as goles_encajados FROM jugadores WHERE nombre='Thibaut Courtois';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("courtois",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posición','partidos_jugados','goles_encajados'],
            "titulo"=>"Thibaut Courtois",
            "enunciado"=>"Real Madrid C.F.",
           
        ]);
    }  public function actionConsulta17(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion)posición,count(distinct pjugados)partidos_jugados,count(distinct num_goles)número_de_goles,count(distinct num_asistencias)número_de_asistencias from jugadores where nombre='Kylian Mbappe';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion as posición,pjugados as partidos_jugados,num_goles as número_de_goles ,num_asistencias as número_de_asistencias FROM jugadores WHERE nombre='Kylian Mbappe';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("mbappe",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posición','partidos_jugados','número_de_goles','número_de_asistencias'],
            "titulo"=>"Kylian Mbappe",
            "enunciado"=>"Paris Saint-Germain",
           
        ]);
    }  
     public function actionConsulta18(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias) from jugadores where nombre='Cristiano Ronaldo';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion,pjugados,num_goles,num_asistencias FROM jugadores WHERE nombre='Cristiano Ronaldo';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("ronaldo",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posicion','pjugados','num_goles','num_asistencias'],
            "titulo"=>"Cristiano Ronaldo",
            "enunciado"=>"Juventus de Turín",
           
        ]);
    }  
    
     public function actionConsulta19(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion)posición,count(distinct pjugados)partidos_jugados,count(distinct num_goles)número_de_goles,count(distinct num_asistencias)número_de_asistencias from jugadores where nombre='Leo Messi';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion as posición,pjugados as partidos_jugados,num_goles as número_de_goles,num_asistencias as número_de_asistencias FROM jugadores WHERE nombre='Leo Messi';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("messi",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posición','partidos_jugados','número_de_goles','número_de_asistencias'],
            "titulo"=>"Lionel Messi",
            "enunciado"=>"F.C Barcelona",
           
        ]);
    }  
     public function actionConsulta20(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias) from jugadores where nombre='Mohamed Salah';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion,pjugados,num_goles,num_asistencias FROM jugadores WHERE nombre='Mohamed Salah';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("salah",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posicion','pjugados','num_goles','num_asistencias'],
            "titulo"=>"Mohamed Salah",
            "enunciado"=>"Liverpool F.C.",
           
        ]);
    }  
     public function actionConsulta21(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias) from jugadores where nombre='Marco Reus';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion,pjugados,num_goles,num_asistencias FROM jugadores WHERE nombre='Marco Reus';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("reus",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posicion','pjugados','num_goles','num_asistencias'],
            "titulo"=>"Marco Reus",
            "enunciado"=>"Borussia Dortmund",
           
        ]);
    }  
     public function actionConsulta22(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias) from jugadores where nombre='Marco Reus';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion,pjugados,num_goles,num_asistencias FROM jugadores WHERE nombre='Marco Reus';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("video1",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posicion','pjugados','num_goles','num_asistencias'],
            "titulo"=>"El gol de Zinedine Zidane en la novena",
            "enunciado"=>"Año 2002",
           
        ]);
    }  
     public function actionConsulta23(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias) from jugadores where nombre='Marco Reus';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion,pjugados,num_goles,num_asistencias FROM jugadores WHERE nombre='Marco Reus';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("video2",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posicion','pjugados','num_goles','num_asistencias'],
            "titulo"=>"Iniesta de mi vida",
            "enunciado"=>"El gol que valió un mundial",
           
        ]);
    }  
     public function actionConsulta24(){
        
            $numero = Yii::$app->db
                ->createCommand("Select count(distinct nombre),count(distinct edad),count(distinct posicion),count(distinct pjugados),count(distinct num_goles),count(distinct num_asistencias) from jugadores where nombre='Marco Reus';")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nombre,edad,posicion,pjugados,num_goles,num_asistencias FROM jugadores WHERE nombre='Marco Reus';",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5000,
            ]
        ]);
        
        return $this->render("video3",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad','posicion','pjugados','num_goles','num_asistencias'],
            "titulo"=>"La decima",
            "enunciado"=>"El gol de Sergio Ramos en el 93",
           
        ]);
    }  
}

